import Vue from "vue";
import VueRouter from "vue-router";
import home from "../components/home.vue";
import projects from "../components/projects.vue";
import projectView from "../components/projectView.vue";
import about from "../components/about.vue";
import award from "../components/award.vue";
import contact from "../components/contact.vue";

import mmenu from "../components/mmenu.vue";

Vue.use(VueRouter);

const routes = [
  { path: "/", name: "home", component: home },
  { path: "/projects/:category?/", name: "projects", component: projects },
  { path: "/project/view/:id?/", name: "projectView", component: projectView },
  { path: "/about", name: "about", component: about },
  { path: "/award", name: "award", component: award },
  { path: "/contact", name: "contact", component: contact },

  { path: "/mmenu", name: "mmenu", component: mmenu }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
