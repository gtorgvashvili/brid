import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
      indx: {}, selectedCategory: {},
      menu: { projects:'Projects', about:'about', award:'Award', contact:'Contact', },
      nav: { expanded: 1, /// nav menu position
          mobileMenu: 0 }, /// mobile menu status },
      lastRoute: 'home',/// for mobile menu
  },
  mutations: {

      setNav: function(state, data){
          if(!isNaN(data.expanded)){
              state.nav.expanded = data.expanded;
          }

          if(!isNaN(data.mobileMenu)){

              state.nav.mobileMenu = data.mobileMenu;
          }
      },

      SetState : function (state, data) {
          // Validate data key & value not equal false
          if (! data.key || ! data.val) return 1;

          Vue.set(state, data.key, data.val);

      },

  },
  actions: {
    indx: async function (context) {


          await fetch('http://www.brid.me/bridadmin/')
                .then((response) => {
                  return response.json();
                })
                .then((data) => {
                        context.state.indx = data;
                });


    }
  },
  modules: {}
});
