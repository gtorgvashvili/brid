import Vue from "vue";

import App from "./App.vue";
import router from "./router";
import store from "./store";


// app.js
import { BootstrapVue, IconsPlugin } from "bootstrap-vue"

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.config.productionTip = false;

// app.js
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '@/assets/mainStyle.css'


// import VueLodash from 'vue-lodash'
// import lodash from 'lodash'
//
// Vue.use(VueLodash, { name: 'custom' , lodash: lodash })


import _ from 'lodash';
Vue.use(_),


new Vue({
  router,
  store,
        created: function () {
          this.$store.dispatch('indx');
          _.get(this.$store.state, 'indx')
        },
  render: h => h(App)
}).$mount("#app");




/**

cd /vuecli/brid
npm run serve

http://www.brid.me/
user: ybid
pass: Hbi13LA1GOwK

DB
host: fietsam.carvingknife.dreamhost.com
user: bbrokers
pass: bbrokersdb
 db:  gbb


 wp admin
 http://www.brid.me/bridadmin/wp-admin/
 user: bridadmin
 pass: lz6%6i$M3GK3mjJb4y

*/